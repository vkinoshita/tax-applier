# Tax applier

Code written by an intern.

With php 7.4, install all dependencies:

```shell
$ composer install
```

## Running tests

```shell
$ vendor/bin/phpunit
```

## TODOS

- Bugfixes
- Refactor
- More test cases
- Documentation
