<?php
declare(strict_types=1);

class TaxApplier
{
    public function apply(float $value, string $state, bool $applyICMS, bool $applyPISCofins): float
    {
        if ($applyPISCofins) {
            $value = $value / 0.91;
        }

        if ($applyICMS) {
            if ($state === 'SP') {
                $value = $value / 0.88;
            } else if ($state === 'RJ') {
                $value = $value / 0.87;
            } else {
                $value = $value / 0.72;
            }
        } else { // Aplicar ISS
            if (!$applyPISCofins) {
                $value = $value * 0.91 / 0.94;
            } else {
                $value = $value * 0.91 / 0.85;
            }
        }

        return $value;
    }
}
