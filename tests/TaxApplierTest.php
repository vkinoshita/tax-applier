<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

class TaxApplierTest extends TestCase
{
    public function test_should_apply_pis_cofins()
    {
        $valor = (new TaxApplier())->apply(100, '', false, true);
        self::assertSame('117.64', $valor);
    }
}
